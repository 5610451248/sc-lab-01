package model;

import gui.SoftwareFrame;

public class BankAccount {
	
		private int balance;
		public int amount;
	
		public BankAccount() {
			balance = 0;			
		}
	
		// add money to your balance
		public void deposit(int amount) {
			balance += amount;  		
		}
		
		// subtract money from your balance
		public boolean withdraw(int amount) {
			int newBalance = balance - amount; 
			if (newBalance >= 0 )  {    
				balance = newBalance;
				return true;
			}
			
			return false;
			
		}	

		public int getBalance() {
			return balance;				
		}	

		SoftwareFrame frame;
	}