package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.omg.CORBA.Environment;

import model.BankAccount;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		frame.setProjectName("ATM");
		frame.setResult("");
		list = new ListenerMgr();
		frame.setListener(list);
		ba = new BankAccount();
		setTestCase();
	}

	public void setTestCase() {

		Balance(0);
		Deposit(200); 
		Withdraw(50);
		Withdraw(170);
		Withdraw(150);
		Deposit(100);
		
	}
	public void Balance(int amount){
		frame.extendResult(("Your balance is >>") + Integer.toString(amount));   
	}
	
	public void Deposit(int amount){
		ba.deposit(amount);  	
		frame.extendResult(("You deposit your money >>") + Integer.toString(amount));  
		frame.extendResult(("Your balance is >>") + Integer.toString(ba.getBalance())); 
		
	}
	
	public void Withdraw(int amount){
		frame.extendResult(("You withdraw your money >>") + Integer.toString(amount)); 
		if (ba.withdraw(amount) == false){
			frame.extendResult("You don't have enough money"); 
		} 
		frame.extendResult(("Your balance is >>") + Integer.toString(ba.getBalance())); 
	}
	
	
	
	BankAccount ba;
	ActionListener list;
	SoftwareFrame frame;
}
